EAPI=2

DESCRIPTION="Puggan Epost server requirments"
KEYWORDS="amd64"
SLOT="0"

RDEPEND="
virtual/mysql
dev-libs/openssl
mail-client/mutt
mail-filter/spamassassin
mail-mta/postfix
net-analyzer/netcat
net-analyzer/nmap
net-analyzer/tcpdump
net-analyzer/tcptraceroute
net-analyzer/traceroute
net-dns/bind-tools
net-mail/courier-imap
mail-filter/pypolicyd-spf
mail-filter/opendkim
mail-filter/spamass-milter
"
