# http://blog.phusion.nl/2012/09/10/mail-in-2012-from-an-admins-perspective/

EAPI=2

DESCRIPTION="Install dependings for user egil"
KEYWORDS="amd64"
SLOT="0"

RDEPEND="
dev-lang/python:2.7[sqlite]
dev-python/pip
dev-python/virtualenv
dev-python/django
net-libs/nodejs
www-apache/mod_wsgi
dev-db/postgis
dev-db/postgresql[server]
"
