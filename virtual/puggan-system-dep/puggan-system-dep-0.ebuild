EAPI=2

DESCRIPTION="Puggan basic system requirements"
KEYWORDS="amd64"
SLOT="0"

RDEPEND="
app-admin/logrotate
app-admin/syslog-ng
app-arch/rar
app-arch/unrar
app-arch/unp
app-arch/zip
app-misc/colordiff
app-misc/screen
app-portage/gentoolkit
media-gfx/icoutils
net-misc/curl
net-misc/dhcpcd
sys-boot/lilo
sys-fs/mdadm
sys-fs/reiserfsprogs
sys-kernel/gentoo-sources
virtual/cron
virtual/jre
virtual/linux-sources
"

