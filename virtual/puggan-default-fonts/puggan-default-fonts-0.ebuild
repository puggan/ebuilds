EAPI=2

DESCRIPTION="Puggan font rekomendated by other applications"
KEYWORDS="amd64"
SLOT="0"

IUSE="unstable"

RDEPEND="
media-fonts/arphicfonts 
media-fonts/bitstream-cyberbit 
media-fonts/droid 
media-fonts/ipamonafont 
media-fonts/ja-ipafonts 
media-fonts/wqy-microhei 
media-fonts/wqy-zenhei
unstable? ( media-fonts/takao-fonts )
media-libs/libtxc_dxtn
"

