EAPI=2

DESCRIPTION="Puggan web server requirments"
KEYWORDS="amd64"
SLOT="0"

RDEPEND="
dev-lang/php[mysql,apache2]
dev-libs/openssl
media-gfx/exiv2
media-gfx/imagemagick
media-libs/gd
net-analyzer/nmap
net-dns/bind-tools
www-client/w3m
www-servers/apache
"
