EAPI=2

DESCRIPTION="Puggans KDE:apps"
KEYWORDS="amd64"
SLOT="0"

RDEPEND="
gnome-base/nautilus
kde-apps/gwenview
kde-apps/kdebase-meta
kde-apps/kdeutils-meta
kde-apps/ksnapshot
kde-apps/kmix
kde-apps/okular
kde-base/kaddressbook
kde-base/kalarm
kde-base/kleopatra
kde-base/kmail
x11-apps/mesa-progs
|| ( x11-base/xorg-x11 x11-base/xorg-server )
"

